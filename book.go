package books

import (
	"context"

	"go.saastack.io/sample/books/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type booksServer struct {
	bookStore pb.BookStore
	bookBLoC  pb.BooksServiceBookServerBLoC
	*pb.BooksServiceBookServerCrud
}

func NewBooksServer(

	bookSt pb.BookStore,

) pb.BooksServer {
	r := &booksServer{

		bookStore: bookSt,
	}

	bookSC := pb.NewBooksServiceBookServerCrud(bookSt, r)
	r.BooksServiceBookServerCrud = bookSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *booksServer) CreateBookBLoC(ctx context.Context, in *pb.CreateBookRequest) error {
	return nil
}

func (s *booksServer) GetBookBLoC(ctx context.Context, in *pb.GetBookRequest) error {
	return nil
}

func (s *booksServer) UpdateBookBLoC(ctx context.Context, in *pb.UpdateBookRequest) error {
	return nil
}

func (s *booksServer) DeleteBookBLoC(ctx context.Context, in *pb.DeleteBookRequest) error {
	return nil
}

func (s *booksServer) BatchGetBookBLoC(ctx context.Context, in *pb.BatchGetBookRequest) error {
	return nil
}

func (s *booksServer) ListBookBLoC(ctx context.Context, in *pb.ListBookRequest) (pb.BookCondition, error) {

	if in.GetParent() != "" {
		return pb.BookFullParentEq{Parent: in.GetParent()}, nil
	}

	//return nil, status.Error(codes.FailedPrecondition, "parent can't be null")
	return pb.TrueCondition{}, nil
}

// These functions are not implemented by CRUDGen, needed to be implemented

func (s *booksServer) AddAuthors(ctx context.Context, in *pb.AddAuthorsRequest) (*pb.Book, error) {

	if err := in.Validate(); err != nil {
		return nil, err
	}

	bk, err := s.bookStore.GetBook(ctx, []string{}, pb.BookIdEq{Id: in.GetBookId()})
	if err != nil {
		return nil, err
	}

	if _, err := s.bookStore.CreateAuthors(ctx, "authors", []string{in.GetBookId()}, in.GetAuthors()...); err != nil {
		return nil, err
	}

	return bk, nil
}
