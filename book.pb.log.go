package books
//
//import (
//	"context"
//
//	"github.com/golang/protobuf/ptypes/empty"
//	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
//	activityLogPb "go.saastack.io/activity-log/pb"
//	"go.saastack.io/sample/books/pb"
//	"go.uber.org/zap"
//)
//
//type logsBooksServer struct {
//	pb.BooksServer
//	actLogCli activityLogPb.ActivityLogsClient
//}
//
//func NewLogsBooksServer(
//	a activityLogPb.ActivityLogsClient,
//	s pb.BooksServer,
//) pb.BooksServer {
//
//	srv := &logsBooksServer{s, a}
//	srv.registerActivityLogFilters()
//
//	return srv
//}
//
//func (s *logsBooksServer) registerActivityLogFilters() {
//
//	if _, err := s.actLogCli.RegisterActivityLogEventFilter(context.Background(), &activityLogPb.RegisterActivityLogEventFilterRequest{
//		Filters: []*activityLogPb.ActivityLogEventFilter{
//
//			{
//				EventName:        ".saastack.books.v1.Books.CreateBook",
//				EventDisplayName: "Create Book",
//			},
//
//			{
//				EventName:        ".saastack.books.v1.Books.GetBook",
//				EventDisplayName: "Get Book",
//			},
//
//			{
//				EventName:        ".saastack.books.v1.Books.DeleteBook",
//				EventDisplayName: "Delete Book",
//			},
//
//			{
//				EventName:        ".saastack.books.v1.Books.UpdateBook",
//				EventDisplayName: "Update Book",
//			},
//
//			{
//				EventName:        ".saastack.books.v1.Books.ListBook",
//				EventDisplayName: "List Book",
//			},
//
//			{
//				EventName:        ".saastack.books.v1.Books.BatchGetBook",
//				EventDisplayName: "Batch Get Book",
//			},
//
//			{
//				EventName:        ".saastack.books.v1.Books.AddAuthors",
//				EventDisplayName: "Add Authors",
//			},
//		},
//	}); err != nil {
//		panic(err)
//	}
//
//	return
//}
//
//func (s *logsBooksServer) CreateBook(ctx context.Context, in *pb.CreateBookRequest) (*pb.Book, error) {
//
//	res, err := s.BooksServer.CreateBook(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.CreateBook",
//		EventDisplayName: "Create Book", // override according to response
//		Message:          "Create Book", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
//
//func (s *logsBooksServer) GetBook(ctx context.Context, in *pb.GetBookRequest) (*pb.Book, error) {
//
//	res, err := s.BooksServer.GetBook(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.GetBook",
//		EventDisplayName: "Get Book", // override according to response
//		Message:          "Get Book", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
//
//func (s *logsBooksServer) DeleteBook(ctx context.Context, in *pb.DeleteBookRequest) (*empty.Empty, error) {
//
//	res, err := s.BooksServer.DeleteBook(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.DeleteBook",
//		EventDisplayName: "Delete Book", // override according to response
//		Message:          "Delete Book", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
//
//func (s *logsBooksServer) UpdateBook(ctx context.Context, in *pb.UpdateBookRequest) (*pb.Book, error) {
//
//	res, err := s.BooksServer.UpdateBook(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.UpdateBook",
//		EventDisplayName: "Update Book", // override according to response
//		Message:          "Update Book", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
//
//func (s *logsBooksServer) ListBook(ctx context.Context, in *pb.ListBookRequest) (*pb.ListBookResponse, error) {
//
//	res, err := s.BooksServer.ListBook(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.ListBook",
//		EventDisplayName: "List Book", // override according to response
//		Message:          "List Book", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
//
//func (s *logsBooksServer) BatchGetBook(ctx context.Context, in *pb.BatchGetBookRequest) (*pb.BatchGetBookResponse, error) {
//
//	res, err := s.BooksServer.BatchGetBook(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.BatchGetBook",
//		EventDisplayName: "Batch Get Book", // override according to response
//		Message:          "Batch Get Book", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
//
//func (s *logsBooksServer) AddAuthors(ctx context.Context, in *pb.AddAuthorsRequest) (*pb.Book, error) {
//
//	res, err := s.BooksServer.AddAuthors(ctx, in)
//	if err != nil {
//		return nil, err
//	}
//
//	log := &activityLogPb.ActivityLog{
//		Id:               "",
//		EventName:        ".saastack.books.v1.Books.AddAuthors",
//		EventDisplayName: "Add Authors", // override according to response
//		Message:          "Add Authors", // override according to response
//		Module:           "Books",
//		ActivityId:       "",
//	}
//	parent := "" // TODO : set this
//
//	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
//		Parent:      parent,
//		ActivityLog: log,
//	}); err != nil {
//		// TODO : set Id
//		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", log.EventName), zap.String("Id", ""))
//	}
//
//	return res, nil
//}
