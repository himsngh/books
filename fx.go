package books

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"go.saastack.io/jaal/schemabuilder"
	"go.saastack.io/sample/books/pb"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

// TODO := Replace "Books" with your service-name
// TODO := Replace "Book" with your object-name

// Module is the fx module encapsulating all the providers of the package
var Module = fx.Options(
	fx.Provide(
		pb.NewPostgresBookStore,

		NewBooksServer,
		fx.Annotated{
			Name: "public",
			Target: func(srv pb.BooksServer) pb.BooksServer {
				return srv
			},
		},

		pb.NewLocalBooksClient,
		fx.Annotated{
			Name: "public",
			Target: func(in struct {
				fx.In
				S pb.BooksServer `name:"public"`
			}) pb.BooksClient {
				return pb.NewLocalBooksClient(in.S)
			},
		},

		fx.Annotated{
			Group:  "grpc-service",
			Target: RegisterGRPCService,
		},
		fx.Annotated{
			Group:  "graphql-service",
			Target: RegisterGraphQLService,
		},
		fx.Annotated{
			Group:  "http-service",
			Target: RegisterHttpService,
		},
	),
	fx.Decorate(
		pb.NewEventsBooksServer,
	),
)

func RegisterGRPCService(in struct {
	fx.In
	Server pb.BooksServer `name:"public"`
}) func(s *grpc.Server) {
	return func(s *grpc.Server) {
		pb.RegisterBooksServer(s, in.Server)
	}
}

func RegisterGraphQLService(in struct {
	fx.In
	Client pb.BooksClient `name:"public"`
}) func(s *schemabuilder.Schema) {
	return func(s *schemabuilder.Schema) {
		pb.RegisterBooksOperations(s, in.Client)
	}
}

func RegisterHttpService(in struct {
	fx.In
	Client pb.BooksClient `name:"public"`
}) func(*runtime.ServeMux, context.Context) error {
	return func(mux *runtime.ServeMux, ctx context.Context) error {
		return pb.RegisterBooksHandlerClient(ctx, mux, in.Client)
	}
}