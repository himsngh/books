package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	BooksCreateBookActivity   = "/saastack.books.v1.Books/CreateBook"
	BooksGetBookActivity      = "/saastack.books.v1.Books/GetBook"
	BooksDeleteBookActivity   = "/saastack.books.v1.Books/DeleteBook"
	BooksUpdateBookActivity   = "/saastack.books.v1.Books/UpdateBook"
	BooksListBookActivity     = "/saastack.books.v1.Books/ListBook"
	BooksBatchGetBookActivity = "/saastack.books.v1.Books/BatchGetBook"
	BooksAddAuthorsActivity   = "/saastack.books.v1.Books/AddAuthors"
)

func RegisterBooksActivities(cli BooksClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateBookRequest) (*Book, error) {
			res, err := cli.CreateBook(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksCreateBookActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetBookRequest) (*Book, error) {
			res, err := cli.GetBook(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksGetBookActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteBookRequest) (*Empty, error) {
			res, err := cli.DeleteBook(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksDeleteBookActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateBookRequest) (*Book, error) {
			res, err := cli.UpdateBook(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksUpdateBookActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListBookRequest) (*ListBookResponse, error) {
			res, err := cli.ListBook(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksListBookActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *BatchGetBookRequest) (*BatchGetBookResponse, error) {
			res, err := cli.BatchGetBook(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksBatchGetBookActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *AddAuthorsRequest) (*Book, error) {
			res, err := cli.AddAuthors(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: BooksAddAuthorsActivity},
	)
}

// BooksActivitiesClient is a typesafe wrapper for BooksActivities.
type BooksActivitiesClient struct {
}

// NewBooksActivitiesClient creates a new BooksActivitiesClient.
func NewBooksActivitiesClient(cli BooksClient) BooksActivitiesClient {
	RegisterBooksActivities(cli)
	return BooksActivitiesClient{}
}

func (ca *BooksActivitiesClient) CreateBook(ctx workflow.Context, in *CreateBookRequest) (*Book, error) {
	future := workflow.ExecuteActivity(ctx, BooksCreateBookActivity, in)
	var result Book
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *BooksActivitiesClient) GetBook(ctx workflow.Context, in *GetBookRequest) (*Book, error) {
	future := workflow.ExecuteActivity(ctx, BooksGetBookActivity, in)
	var result Book
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *BooksActivitiesClient) DeleteBook(ctx workflow.Context, in *DeleteBookRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, BooksDeleteBookActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *BooksActivitiesClient) UpdateBook(ctx workflow.Context, in *UpdateBookRequest) (*Book, error) {
	future := workflow.ExecuteActivity(ctx, BooksUpdateBookActivity, in)
	var result Book
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *BooksActivitiesClient) ListBook(ctx workflow.Context, in *ListBookRequest) (*ListBookResponse, error) {
	future := workflow.ExecuteActivity(ctx, BooksListBookActivity, in)
	var result ListBookResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *BooksActivitiesClient) BatchGetBook(ctx workflow.Context, in *BatchGetBookRequest) (*BatchGetBookResponse, error) {
	future := workflow.ExecuteActivity(ctx, BooksBatchGetBookActivity, in)
	var result BatchGetBookResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *BooksActivitiesClient) AddAuthors(ctx workflow.Context, in *AddAuthorsRequest) (*Book, error) {
	future := workflow.ExecuteActivity(ctx, BooksAddAuthorsActivity, in)
	var result Book
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
