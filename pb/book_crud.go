package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BooksServiceBookServerCrud struct {
	store BookStore
	bloc  BooksServiceBookServerBLoC
}

type BooksServiceBookServerBLoC interface {
	CreateBookBLoC(context.Context, *CreateBookRequest) error

	GetBookBLoC(context.Context, *GetBookRequest) error

	UpdateBookBLoC(context.Context, *UpdateBookRequest) error

	DeleteBookBLoC(context.Context, *DeleteBookRequest) error

	BatchGetBookBLoC(context.Context, *BatchGetBookRequest) error

	ListBookBLoC(context.Context, *ListBookRequest) (BookCondition, error)
}

func NewBooksServiceBookServerCrud(s BookStore, b BooksServiceBookServerBLoC) *BooksServiceBookServerCrud {
	return &BooksServiceBookServerCrud{store: s, bloc: b}
}

func (s *BooksServiceBookServerCrud) CreateBook(ctx context.Context, in *CreateBookRequest) (*Book, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateBookBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.Book.Id) != in.Book.GetPrefix() {
		in.Book.Id = in.Parent
	}

	ids, err := s.store.CreateBooks(ctx, in.Book)
	if err != nil {
		return nil, err
	}

	in.Book.Id = ids[0]

	return in.GetBook(), nil
}

func (s *BooksServiceBookServerCrud) UpdateBook(ctx context.Context, in *UpdateBookRequest) (*Book, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetBook().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateBookBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateBook(ctx,
		in.Book, mask,
		BookIdEq{Id: in.Book.Id},
	); err != nil {
		return nil, err
	}

	updatedBook, err := s.store.GetBook(ctx, []string{},
		BookIdEq{
			Id: in.GetBook().GetId(),
		},
	)
	if err != nil {
		return nil, err
	}

	return updatedBook, nil
}

func (s *BooksServiceBookServerCrud) GetBook(ctx context.Context, in *GetBookRequest) (*Book, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetBookBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	res, err := s.store.GetBook(ctx, mask, BookIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Book not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *BooksServiceBookServerCrud) ListBook(ctx context.Context, in *ListBookRequest) (*ListBookResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	condition, err := s.bloc.ListBookBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	return s.ListWithoutPagination(ctx, condition, mask)
}

func (s *BooksServiceBookServerCrud) ListWithoutPagination(ctx context.Context, condition BookCondition, viewMask []string) (*ListBookResponse, error) {

	list, err := s.store.ListBooks(ctx,
		viewMask,
		condition,
	)
	if err != nil {
		return nil, err
	}

	return &ListBookResponse{Book: list}, err
}

func (s *BooksServiceBookServerCrud) DeleteBook(ctx context.Context, in *DeleteBookRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteBookBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteBook(ctx, BookIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *BooksServiceBookServerCrud) BatchGetBook(ctx context.Context, in *BatchGetBookRequest) (*BatchGetBookResponse, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.BatchGetBookBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	getIds := make([]string, 0, len(in.Ids))
	for _, id := range in.Ids {
		getIds = append(getIds, idutil.GetId(id))
	}

	mask := s.GetViewMask(in.ViewMask)

	list, err := s.store.ListBooks(ctx, mask, BookIdIn{Id: getIds})
	if err != nil {
		return nil, err
	}

	resultMap := make(map[string]*Book, 0)
	for i, it := range list {
		_ = i

		resultMap[it.Id] = it
	}

	isGrpc := userinfo.IsGrpcCall(ctx)

	result := make([]*Book, 0, len(in.Ids))
	for _, id := range in.Ids {
		if resultMap[id] == nil && isGrpc {
			result = append(result, &Book{})
			continue
		}
		result = append(result, resultMap[id])
	}

	return &BatchGetBookResponse{Book: result}, nil
}

func (s *BooksServiceBookServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
