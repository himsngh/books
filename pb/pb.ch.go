package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
)

var objectTableMap = chaku_globals.ObjectTable{
	"book": {
		"id":          "book",
		"title":       "book",
		"description": "book",
		"genre":       "book",
		"authors":     "author",
	},
	"author": {
		"id":         "author",
		"first_name": "author",
		"last_name":  "author",
		"email":      "author",
	},
}

func (m *Book) PackageName() string {
	return "saastack_books_v1"
}

func (m *Book) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Book) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	case "authors":
		if len(m.Authors) == 0 {
			m.Authors = append(m.Authors, &Author{})
		}
		return m.Authors[0], nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Book) ObjectName() string {
	return "book"
}

func (m *Book) Fields() []string {
	return []string{
		"id", "title", "description", "genre", "authors",
	}
}

func (m *Book) IsObject(field string) bool {
	switch field {
	case "authors":
		return true
	default:
		return false
	}
}

func (m *Book) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "authors":
		sli := make([]driver.Descriptor, 0, len(m.Authors))
		for _, c := range m.Authors {
			sli = append(sli, c)
		}
		return sli, nil
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Book) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "title":
		return m.Title, nil
	case "description":
		return m.Description, nil
	case "genre":
		return m.Genre, nil
	case "authors":
		return m.Authors, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Book) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "title":
		return &m.Title, nil
	case "description":
		return &m.Description, nil
	case "genre":
		return &m.Genre, nil
	case "authors":
		return &m.Authors, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Book) New(field string) error {
	switch field {
	case "id":
		return nil
	case "title":
		return nil
	case "description":
		return nil
	case "genre":
		return nil
	case "authors":
		if m.Authors == nil {
			m.Authors = make([]*Author, 0)
		}
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Book) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "title":
		return "string"
	case "description":
		return "string"
	case "genre":
		return "enum"
	case "authors":
		return "repeated"
	default:
		return ""
	}
}

func (_ *Book) GetEmptyObject() (m *Book) {
	m = &Book{}
	_ = m.New("authors")
	m.Authors = append(m.Authors, &Author{})
	m.Authors[0].GetEmptyObject()
	return
}

func (m *Book) GetPrefix() string {
	return "boo"
}

func (m *Book) GetID() string {
	return m.Id
}

func (m *Book) SetID(id string) {
	m.Id = id
}

func (m *Book) IsRoot() bool {
	return true
}

func (m *Book) IsFlatObject(f string) bool {
	return false
}

func (m *Author) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) ObjectName() string {
	return "author"
}

func (m *Author) Fields() []string {
	return []string{
		"id", "first_name", "last_name", "email",
	}
}

func (m *Author) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Author) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Author) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "first_name":
		return m.FirstName, nil
	case "last_name":
		return m.LastName, nil
	case "email":
		return m.Email, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "first_name":
		return &m.FirstName, nil
	case "last_name":
		return &m.LastName, nil
	case "email":
		return &m.Email, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) New(field string) error {
	switch field {
	case "id":
		return nil
	case "first_name":
		return nil
	case "last_name":
		return nil
	case "email":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Author) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "first_name":
		return "string"
	case "last_name":
		return "string"
	case "email":
		return "string"
	default:
		return ""
	}
}

func (_ *Author) GetEmptyObject() (m *Author) {
	m = &Author{}
	return
}

func (m *Author) GetPrefix() string {
	return "aut"
}

func (m *Author) GetID() string {
	return m.Id
}

func (m *Author) SetID(id string) {
	m.Id = id
}

func (m *Author) IsRoot() bool {
	return false
}

func (m *Author) IsFlatObject(f string) bool {
	return false
}

func (m *Book) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	case "author":
		return 1
	}
	return 0
}

type BookStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s BookStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s BookStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewBookStore(d driver.Driver) BookStore {
	return BookStore{d: d, limitMultiplier: 1}
}

func NewPostgresBookStore(db *x.DB, usr driver.IUserInfo) BookStore {
	return BookStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type BookTx struct {
	BookStore
}

func (s BookStore) BeginTx(ctx context.Context) (*BookTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &BookTx{
		BookStore: BookStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *BookTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *BookTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s BookStore) CreateBookPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_books_v1;
CREATE TABLE IF NOT EXISTS  saastack_books_v1.book( id text DEFAULT ''::text , title text DEFAULT ''::text , description text DEFAULT ''::text , genre integer DEFAULT 0 , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_books_v1.author( id text DEFAULT ''::text , first_name text DEFAULT ''::text , last_name text DEFAULT ''::text , email text DEFAULT ''::text , p0id text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, p0id)); 
CREATE TABLE IF NOT EXISTS  saastack_books_v1.book_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s BookStore) CreateBooks(ctx context.Context, list ...*Book) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Book{}, &Book{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Book{}, &Book{}, "", []string{})
}

func (s BookStore) DeleteBook(ctx context.Context, cond BookCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.bookCondToDriverBookCond(s.d), &Book{}, &Book{})
	}
	return s.d.Delete(ctx, cond.bookCondToDriverBookCond(s.d), &Book{}, &Book{})
}

func (s BookStore) UpdateBook(ctx context.Context, req *Book, fields []string, cond BookCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.bookCondToDriverBookCond(s.d), req, &Book{}, fields...)
	}
	return s.d.Update(ctx, cond.bookCondToDriverBookCond(s.d), req, &Book{}, fields...)
}

func (s BookStore) UpdateBookMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Book{}, &Book{}, list...)
}

func (s BookStore) GetBook(ctx context.Context, fields []string, cond BookCondition, opt ...getBooksOption) (*Book, error) {
	if len(fields) == 0 {
		fields = (&Book{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listBooksOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListBooks(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s BookStore) ListBooks(ctx context.Context, fields []string, cond BookCondition, opt ...listBooksOption) ([]*Book, error) {
	if len(fields) == 0 {
		fields = (&Book{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetBookCondition == nil {
					page.SetBookCondition = defaultSetBookCondition
				}
				cond = page.SetBookCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.bookCondToDriverBookCond(s.d), &Book{}, &Book{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.bookCondToDriverBookCond(s.d), &Book{}, &Book{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Book, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Book{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperBook(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s BookStore) CountBooks(ctx context.Context, cond BookCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.bookCondToDriverBookCond(s.d), &Book{}, &Book{})
}

type getBooksOption interface {
	getOptBooks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptBooks() { // method of no significant use
}

type listBooksOption interface {
	listOptBooks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptBooks() {
}

func (OrderBy) listOptBooks() {
}

func (*CursorBasedPagination) listOptBooks() {
}

func defaultSetBookCondition(upOrDown bool, cursor string, cond BookCondition) BookCondition {
	if upOrDown {
		if cursor != "" {
			return BookAnd{cond, BookIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return BookAnd{cond, BookIdGt{cursor}}
	}
	return cond
}

type BookAnd []BookCondition

func (p BookAnd) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.bookCondToDriverBookCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type BookOr []BookCondition

func (p BookOr) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.bookCondToDriverBookCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type BookParentEq struct {
	Parent string
}

func (c BookParentEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookFullParentEq struct {
	Parent string
}

func (c BookFullParentEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookParentNotEq struct {
	Parent string
}

func (c BookParentNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookFullParentNotEq struct {
	Parent string
}

func (c BookFullParentNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookParentLike struct {
	Parent string
}

func (c BookParentLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookFullParentLike struct {
	Parent string
}

func (c BookFullParentLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookParentILike struct {
	Parent string
}

func (c BookParentILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookFullParentILike struct {
	Parent string
}

func (c BookFullParentILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookParentIn struct {
	Parent []string
}

func (c BookParentIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookFullParentIn struct {
	Parent []string
}

func (c BookFullParentIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookParentNotIn struct {
	Parent []string
}

func (c BookParentNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookFullParentNotIn struct {
	Parent []string
}

func (c BookFullParentNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdEq struct {
	Id string
}

func (c BookIdEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleEq struct {
	Title string
}

func (c BookTitleEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionEq struct {
	Description string
}

func (c BookDescriptionEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreEq struct {
	Genre Genre
}

func (c BookGenreEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdEq struct {
	Id string
}

func (c BookAuthorIdEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameEq struct {
	FirstName string
}

func (c BookAuthorFirstNameEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameEq struct {
	LastName string
}

func (c BookAuthorLastNameEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailEq struct {
	Email string
}

func (c BookAuthorEmailEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdNotEq struct {
	Id string
}

func (c BookIdNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleNotEq struct {
	Title string
}

func (c BookTitleNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionNotEq struct {
	Description string
}

func (c BookDescriptionNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreNotEq struct {
	Genre Genre
}

func (c BookGenreNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdNotEq struct {
	Id string
}

func (c BookAuthorIdNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameNotEq struct {
	FirstName string
}

func (c BookAuthorFirstNameNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameNotEq struct {
	LastName string
}

func (c BookAuthorLastNameNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailNotEq struct {
	Email string
}

func (c BookAuthorEmailNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdGt struct {
	Id string
}

func (c BookIdGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleGt struct {
	Title string
}

func (c BookTitleGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionGt struct {
	Description string
}

func (c BookDescriptionGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreGt struct {
	Genre Genre
}

func (c BookGenreGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdGt struct {
	Id string
}

func (c BookAuthorIdGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameGt struct {
	FirstName string
}

func (c BookAuthorFirstNameGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameGt struct {
	LastName string
}

func (c BookAuthorLastNameGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailGt struct {
	Email string
}

func (c BookAuthorEmailGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdLt struct {
	Id string
}

func (c BookIdLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleLt struct {
	Title string
}

func (c BookTitleLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionLt struct {
	Description string
}

func (c BookDescriptionLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreLt struct {
	Genre Genre
}

func (c BookGenreLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdLt struct {
	Id string
}

func (c BookAuthorIdLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameLt struct {
	FirstName string
}

func (c BookAuthorFirstNameLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameLt struct {
	LastName string
}

func (c BookAuthorLastNameLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailLt struct {
	Email string
}

func (c BookAuthorEmailLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdGtOrEq struct {
	Id string
}

func (c BookIdGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleGtOrEq struct {
	Title string
}

func (c BookTitleGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionGtOrEq struct {
	Description string
}

func (c BookDescriptionGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreGtOrEq struct {
	Genre Genre
}

func (c BookGenreGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdGtOrEq struct {
	Id string
}

func (c BookAuthorIdGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameGtOrEq struct {
	FirstName string
}

func (c BookAuthorFirstNameGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameGtOrEq struct {
	LastName string
}

func (c BookAuthorLastNameGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailGtOrEq struct {
	Email string
}

func (c BookAuthorEmailGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdLtOrEq struct {
	Id string
}

func (c BookIdLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleLtOrEq struct {
	Title string
}

func (c BookTitleLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionLtOrEq struct {
	Description string
}

func (c BookDescriptionLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreLtOrEq struct {
	Genre Genre
}

func (c BookGenreLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdLtOrEq struct {
	Id string
}

func (c BookAuthorIdLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameLtOrEq struct {
	FirstName string
}

func (c BookAuthorFirstNameLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameLtOrEq struct {
	LastName string
}

func (c BookAuthorLastNameLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailLtOrEq struct {
	Email string
}

func (c BookAuthorEmailLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdLike struct {
	Id string
}

func (c BookIdLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleLike struct {
	Title string
}

func (c BookTitleLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionLike struct {
	Description string
}

func (c BookDescriptionLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdLike struct {
	Id string
}

func (c BookAuthorIdLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameLike struct {
	FirstName string
}

func (c BookAuthorFirstNameLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameLike struct {
	LastName string
}

func (c BookAuthorLastNameLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailLike struct {
	Email string
}

func (c BookAuthorEmailLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdILike struct {
	Id string
}

func (c BookIdILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleILike struct {
	Title string
}

func (c BookTitleILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionILike struct {
	Description string
}

func (c BookDescriptionILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdILike struct {
	Id string
}

func (c BookAuthorIdILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameILike struct {
	FirstName string
}

func (c BookAuthorFirstNameILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameILike struct {
	LastName string
}

func (c BookAuthorLastNameILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailILike struct {
	Email string
}

func (c BookAuthorEmailILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeleted struct {
	IsDeleted bool
}

func (c BookDeleted) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorDeleted struct {
	IsDeleted bool
}

func (c BookAuthorDeleted) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByEq struct {
	By string
}

func (c BookCreatedByEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByNotEq struct {
	By string
}

func (c BookCreatedByNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByGt struct {
	By string
}

func (c BookCreatedByGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByLt struct {
	By string
}

func (c BookCreatedByLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByGtOrEq struct {
	By string
}

func (c BookCreatedByGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByLtOrEq struct {
	By string
}

func (c BookCreatedByLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByLike struct {
	By string
}

func (c BookCreatedByLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookCreatedByILike struct {
	By string
}

func (c BookCreatedByILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByEq struct {
	By string
}

func (c BookUpdatedByEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByNotEq struct {
	By string
}

func (c BookUpdatedByNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByGt struct {
	By string
}

func (c BookUpdatedByGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByLt struct {
	By string
}

func (c BookUpdatedByLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByGtOrEq struct {
	By string
}

func (c BookUpdatedByGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByLtOrEq struct {
	By string
}

func (c BookUpdatedByLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByLike struct {
	By string
}

func (c BookUpdatedByLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookUpdatedByILike struct {
	By string
}

func (c BookUpdatedByILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByEq struct {
	By string
}

func (c BookDeletedByEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByNotEq struct {
	By string
}

func (c BookDeletedByNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnNotEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByGt struct {
	By string
}

func (c BookDeletedByGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnGt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByLt struct {
	By string
}

func (c BookDeletedByLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnLt) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByGtOrEq struct {
	By string
}

func (c BookDeletedByGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnGtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByLtOrEq struct {
	By string
}

func (c BookDeletedByLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnLtOrEq) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByLike struct {
	By string
}

func (c BookDeletedByLike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDeletedByILike struct {
	By string
}

func (c BookDeletedByILike) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdIn struct {
	Id []string
}

func (c BookIdIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleIn struct {
	Title []string
}

func (c BookTitleIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionIn struct {
	Description []string
}

func (c BookDescriptionIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreIn struct {
	Genre []Genre
}

func (c BookGenreIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdIn struct {
	Id []string
}

func (c BookAuthorIdIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameIn struct {
	FirstName []string
}

func (c BookAuthorFirstNameIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameIn struct {
	LastName []string
}

func (c BookAuthorLastNameIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailIn struct {
	Email []string
}

func (c BookAuthorEmailIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookIdNotIn struct {
	Id []string
}

func (c BookIdNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookTitleNotIn struct {
	Title []string
}

func (c BookTitleNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookDescriptionNotIn struct {
	Description []string
}

func (c BookDescriptionNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "description", Value: c.Description, Operator: d, Descriptor: &Book{}, FieldMask: "description", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookGenreNotIn struct {
	Genre []Genre
}

func (c BookGenreNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorIdNotIn struct {
	Id []string
}

func (c BookAuthorIdNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "authors.id", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorFirstNameNotIn struct {
	FirstName []string
}

func (c BookAuthorFirstNameNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorLastNameNotIn struct {
	LastName []string
}

func (c BookAuthorLastNameNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "authors.last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

type BookAuthorEmailNotIn struct {
	Email []string
}

func (c BookAuthorEmailNotIn) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "authors.email", RootDescriptor: &Book{}, CurrentDescriptor: &Book{}}
}

func (c TrueCondition) bookCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type bookMapperObject struct {
	id          string
	title       string
	description string
	genre       Genre
	authors     map[string]*bookAuthorMapperObject
}

func (s *bookMapperObject) GetUniqueIdentifier() string {
	return s.id
}

type bookAuthorMapperObject struct {
	id        string
	firstName string
	lastName  string
	email     string
}

func (s *bookAuthorMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperBook(rows []*Book) []*Book {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedBookMappers := map[string]*bookMapperObject{}

	for _, rw := range rows {

		tempBook := &bookMapperObject{}
		tempBookAuthor := &bookAuthorMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		if len(rw.Authors) == 0 {
			_ = rw.New("authors")
			rw.Authors = append(rw.Authors, &Author{})
			rw.Authors[0] = rw.Authors[0].GetEmptyObject()
		}
		tempBookAuthor.id = rw.Authors[0].Id
		tempBookAuthor.firstName = rw.Authors[0].FirstName
		tempBookAuthor.lastName = rw.Authors[0].LastName
		tempBookAuthor.email = rw.Authors[0].Email

		tempBook.id = rw.Id
		tempBook.title = rw.Title
		tempBook.description = rw.Description
		tempBook.genre = rw.Genre
		tempBook.authors = map[string]*bookAuthorMapperObject{
			tempBookAuthor.GetUniqueIdentifier(): tempBookAuthor,
		}

		if combinedBookMappers[tempBook.GetUniqueIdentifier()] == nil {
			combinedBookMappers[tempBook.GetUniqueIdentifier()] = tempBook
		} else {

			bookMapper := combinedBookMappers[tempBook.GetUniqueIdentifier()]

			if bookMapper.authors[tempBookAuthor.GetUniqueIdentifier()] == nil {
				bookMapper.authors[tempBookAuthor.GetUniqueIdentifier()] = tempBookAuthor
			}
			combinedBookMappers[tempBook.GetUniqueIdentifier()] = bookMapper
		}

	}

	combinedBooks := make(map[string]*Book, 0)

	for _, book := range combinedBookMappers {
		tempBook := &Book{}
		tempBook.Id = book.id
		tempBook.Title = book.title
		tempBook.Description = book.description
		tempBook.Genre = book.genre

		combinedBookAuthors := []*Author{}

		for _, bookAuthor := range book.authors {
			tempBookAuthor := &Author{}
			tempBookAuthor.Id = bookAuthor.id
			tempBookAuthor.FirstName = bookAuthor.firstName
			tempBookAuthor.LastName = bookAuthor.lastName
			tempBookAuthor.Email = bookAuthor.email

			if tempBookAuthor.Id == "" {
				continue
			}

			combinedBookAuthors = append(combinedBookAuthors, tempBookAuthor)

		}
		tempBook.Authors = combinedBookAuthors

		if tempBook.Id == "" {
			continue
		}

		combinedBooks[tempBook.Id] = tempBook

	}
	list := make([]*Book, 0, len(combinedBooks))
	for _, i := range ids {
		list = append(list, combinedBooks[i])
	}
	return list
}

func (s BookStore) CreateAuthors(ctx context.Context, objFieldMask string, pid []string, list ...*Author) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Author{}, &Book{}, objFieldMask, pid)
	}
	return s.d.Insert(ctx, vv, &Author{}, &Book{}, objFieldMask, pid)
}

func (s BookStore) DeleteAuthor(ctx context.Context, cond AuthorCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.authorCondToDriverBookCond(s.d), &Author{}, &Book{})
	}
	return s.d.Delete(ctx, cond.authorCondToDriverBookCond(s.d), &Author{}, &Book{})
}

func (s BookStore) UpdateAuthor(ctx context.Context, req *Author, fields []string, cond AuthorCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.authorCondToDriverBookCond(s.d), req, &Book{}, fields...)
	}
	return s.d.Update(ctx, cond.authorCondToDriverBookCond(s.d), req, &Book{}, fields...)
}

func (s BookStore) UpdateAuthorMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Author{}, &Book{}, list...)
}

func (s BookStore) GetAuthor(ctx context.Context, fields []string, cond AuthorCondition, opt ...getAuthorsBookOption) (*Author, error) {
	if len(fields) == 0 {
		fields = (&Author{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listAuthorsBookOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListAuthors(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s BookStore) ListAuthors(ctx context.Context, fields []string, cond AuthorCondition, opt ...listAuthorsBookOption) ([]*Author, error) {
	if len(fields) == 0 {
		fields = (&Author{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetAuthorCondition == nil {
					page.SetAuthorCondition = defaultSetAuthorCondition
				}
				cond = page.SetAuthorCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.authorCondToDriverBookCond(s.d), &Author{}, &Book{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.authorCondToDriverBookCond(s.d), &Author{}, &Book{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Author, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Author{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperAuthor(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s BookStore) CountAuthors(ctx context.Context, cond AuthorCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.authorCondToDriverBookCond(s.d), &Author{}, &Book{})
}

type getAuthorsBookOption interface {
	getOptAuthorsBook() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptAuthorsBook() { // method of no significant use
}

type listAuthorsBookOption interface {
	listOptAuthorsBook() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptAuthorsBook() {
}

func (OrderBy) listOptAuthorsBook() {
}

func (*CursorBasedPagination) listOptAuthorsBook() {
}

func defaultSetAuthorCondition(upOrDown bool, cursor string, cond AuthorCondition) AuthorCondition {
	if upOrDown {
		if cursor != "" {
			return AuthorAnd{cond, AuthorIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return AuthorAnd{cond, AuthorIdGt{cursor}}
	}
	return cond
}

type AuthorAnd []AuthorCondition

func (p AuthorAnd) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.authorCondToDriverBookCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type AuthorOr []AuthorCondition

func (p AuthorOr) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.authorCondToDriverBookCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type AuthorParentEq struct {
	Parent string
}

func (c AuthorParentEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorParentNotEq struct {
	Parent string
}

func (c AuthorParentNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorParentLike struct {
	Parent string
}

func (c AuthorParentLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorParentILike struct {
	Parent string
}

func (c AuthorParentILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorParentIn struct {
	Parent []string
}

func (c AuthorParentIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorParentNotIn struct {
	Parent []string
}

func (c AuthorParentNotIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdEq struct {
	Id string
}

func (c AuthorIdEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameEq struct {
	FirstName string
}

func (c AuthorFirstNameEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameEq struct {
	LastName string
}

func (c AuthorLastNameEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailEq struct {
	Email string
}

func (c AuthorEmailEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdNotEq struct {
	Id string
}

func (c AuthorIdNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameNotEq struct {
	FirstName string
}

func (c AuthorFirstNameNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameNotEq struct {
	LastName string
}

func (c AuthorLastNameNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailNotEq struct {
	Email string
}

func (c AuthorEmailNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdGt struct {
	Id string
}

func (c AuthorIdGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameGt struct {
	FirstName string
}

func (c AuthorFirstNameGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameGt struct {
	LastName string
}

func (c AuthorLastNameGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailGt struct {
	Email string
}

func (c AuthorEmailGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdLt struct {
	Id string
}

func (c AuthorIdLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameLt struct {
	FirstName string
}

func (c AuthorFirstNameLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameLt struct {
	LastName string
}

func (c AuthorLastNameLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailLt struct {
	Email string
}

func (c AuthorEmailLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdGtOrEq struct {
	Id string
}

func (c AuthorIdGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameGtOrEq struct {
	FirstName string
}

func (c AuthorFirstNameGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameGtOrEq struct {
	LastName string
}

func (c AuthorLastNameGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailGtOrEq struct {
	Email string
}

func (c AuthorEmailGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdLtOrEq struct {
	Id string
}

func (c AuthorIdLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameLtOrEq struct {
	FirstName string
}

func (c AuthorFirstNameLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameLtOrEq struct {
	LastName string
}

func (c AuthorLastNameLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailLtOrEq struct {
	Email string
}

func (c AuthorEmailLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdLike struct {
	Id string
}

func (c AuthorIdLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameLike struct {
	FirstName string
}

func (c AuthorFirstNameLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameLike struct {
	LastName string
}

func (c AuthorLastNameLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailLike struct {
	Email string
}

func (c AuthorEmailLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdILike struct {
	Id string
}

func (c AuthorIdILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameILike struct {
	FirstName string
}

func (c AuthorFirstNameILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameILike struct {
	LastName string
}

func (c AuthorLastNameILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailILike struct {
	Email string
}

func (c AuthorEmailILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeleted struct {
	IsDeleted bool
}

func (c AuthorDeleted) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByEq struct {
	By string
}

func (c AuthorCreatedByEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByNotEq struct {
	By string
}

func (c AuthorCreatedByNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByGt struct {
	By string
}

func (c AuthorCreatedByGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByLt struct {
	By string
}

func (c AuthorCreatedByLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByGtOrEq struct {
	By string
}

func (c AuthorCreatedByGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByLtOrEq struct {
	By string
}

func (c AuthorCreatedByLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByLike struct {
	By string
}

func (c AuthorCreatedByLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorCreatedByILike struct {
	By string
}

func (c AuthorCreatedByILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByEq struct {
	By string
}

func (c AuthorUpdatedByEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByNotEq struct {
	By string
}

func (c AuthorUpdatedByNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByGt struct {
	By string
}

func (c AuthorUpdatedByGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByLt struct {
	By string
}

func (c AuthorUpdatedByLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByGtOrEq struct {
	By string
}

func (c AuthorUpdatedByGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByLtOrEq struct {
	By string
}

func (c AuthorUpdatedByLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByLike struct {
	By string
}

func (c AuthorUpdatedByLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorUpdatedByILike struct {
	By string
}

func (c AuthorUpdatedByILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByEq struct {
	By string
}

func (c AuthorDeletedByEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByNotEq struct {
	By string
}

func (c AuthorDeletedByNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnNotEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByGt struct {
	By string
}

func (c AuthorDeletedByGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnGt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByLt struct {
	By string
}

func (c AuthorDeletedByLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnLt) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByGtOrEq struct {
	By string
}

func (c AuthorDeletedByGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnGtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByLtOrEq struct {
	By string
}

func (c AuthorDeletedByLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnLtOrEq) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByLike struct {
	By string
}

func (c AuthorDeletedByLike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorDeletedByILike struct {
	By string
}

func (c AuthorDeletedByILike) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdIn struct {
	Id []string
}

func (c AuthorIdIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameIn struct {
	FirstName []string
}

func (c AuthorFirstNameIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameIn struct {
	LastName []string
}

func (c AuthorLastNameIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailIn struct {
	Email []string
}

func (c AuthorEmailIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorIdNotIn struct {
	Id []string
}

func (c AuthorIdNotIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorFirstNameNotIn struct {
	FirstName []string
}

func (c AuthorFirstNameNotIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &Author{}, FieldMask: "first_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorLastNameNotIn struct {
	LastName []string
}

func (c AuthorLastNameNotIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &Author{}, FieldMask: "last_name", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

type AuthorEmailNotIn struct {
	Email []string
}

func (c AuthorEmailNotIn) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}, CurrentDescriptor: &Author{}}
}

func (c TrueCondition) authorCondToDriverBookCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type authorMapperObject struct {
	id        string
	firstName string
	lastName  string
	email     string
}

func (s *authorMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperAuthor(rows []*Author) []*Author {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedAuthorMappers := map[string]*authorMapperObject{}

	for _, rw := range rows {

		tempAuthor := &authorMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempAuthor.id = rw.Id
		tempAuthor.firstName = rw.FirstName
		tempAuthor.lastName = rw.LastName
		tempAuthor.email = rw.Email

		if combinedAuthorMappers[tempAuthor.GetUniqueIdentifier()] == nil {
			combinedAuthorMappers[tempAuthor.GetUniqueIdentifier()] = tempAuthor
		}
	}

	combinedAuthors := make(map[string]*Author, 0)

	for _, author := range combinedAuthorMappers {
		tempAuthor := &Author{}
		tempAuthor.Id = author.id
		tempAuthor.FirstName = author.firstName
		tempAuthor.LastName = author.lastName
		tempAuthor.Email = author.email

		if tempAuthor.Id == "" {
			continue
		}

		combinedAuthors[tempAuthor.Id] = tempAuthor

	}
	list := make([]*Author, 0, len(combinedAuthors))
	for _, i := range ids {
		list = append(list, combinedAuthors[i])
	}
	return list
}

func (m *Book) IsUsedMultipleTimes(f string) bool {
	return false
}

type TrueCondition struct{}

type BookCondition interface {
	bookCondToDriverBookCond(d driver.Driver) driver.Conditioner
}
type AuthorCondition interface {
	authorCondToDriverBookCond(d driver.Driver) driver.Conditioner
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// All pagination-cursor condition functions for different objects
	// SetBookCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetBookCondition func(upOrDown bool, cursor string, cond BookCondition) BookCondition
	// SetAuthorCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetAuthorCondition func(upOrDown bool, cursor string, cond AuthorCondition) AuthorCondition

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type OrderBy struct {
	Bys []driver.OrderByType
}

func (o OrderBy) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_OrderBy, o
}
